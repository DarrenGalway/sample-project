const food = [
    {
        name: 'Sausage and Pepper Penne',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=20',
        largeImage: 'https://unsplash.it/600/338?image=20',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?',
        featured: 'true'
    }, {
        name: 'Pita Pizza',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=30',
        largeImage: 'https://unsplash.it/600/338?image=30',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?',
        featured: 'true'
    }, {
        name: 'Taco Pasta',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=40',
        largeImage: 'https://unsplash.it/600/338?image=40',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?',
        featured: 'true'
    }, {
        name: 'Greek Salad',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=50',
        largeImage: 'https://unsplash.it/600/338?image=50',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?'
    }, {
        name: 'Steak and Potatos',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=60',
        largeImage: 'https://unsplash.it/600/338?image=60',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?'
    }, {
        name: 'Burrito Bowl',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=70',
        largeImage: 'https://unsplash.it/600/338?image=70',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?'
    }, {
        name: 'Tacos',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=80',
        largeImage: 'https://unsplash.it/600/338?image=80',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?'
    }, {
        name: 'Tacos',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=89',
        largeImage: 'https://unsplash.it/600/338?image=89',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?'
    }, {
        name: 'Tacos',
        link: 'http://darrengalway.com',
        type: 'Mexican',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatibus rerum hic quod, tenetur accusantium molestiae maxime vero fugiat eius quas sunt, veniam culpa placeat ea unde optio enim soluta.',
        ingredients: ['1b Sausage', '2 Green Peppers', 'Tomato Sauce', '3 Onions'],
        image: 'https://unsplash.it/50/50?image=82',
        largeImage: 'https://unsplash.it/600/338?image=82',
        avgCost: '$37.00',
        cookTime: '2h',
        shortDesc: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, ratione?'
    }
]

export default food
