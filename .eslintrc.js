module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  extends: 'airbnb-base',
  // required to lint *.vue files
  plugins: [
    'html'
  ],
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'build/webpack.base.conf.js'
      }
    }
  },
  // add your custom rules here
  'rules': {
    "no-comma-dangle": 0,
    "comma-dangle": 0,
    "no-new": 0,
    "semi": 0,
    "indent": [2, 4],
    "no-console": 0,
    "no-undef": 0,
    "no-unused-vars": 0,
    "no-underscore-dangle": 0,
    "arrow-body-style": 0,
    "no-param-reassign": 0,
    "no-shadow": 0,
    "no-confusing-arrow": 0,
    "new-cap": 0,
    "no-lonely-if": 0,
    "max-len": 0,
    "no-prototype-builtins": 0,
    "no-return-assign": 0,
    "object-shorthand": 0,
    "no-alert": 0,
    // don't require .vue extension when importing
    'import/extensions': ['error', 'always', {
      'js': 'never',
      'vue': 'never'
    }],
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
